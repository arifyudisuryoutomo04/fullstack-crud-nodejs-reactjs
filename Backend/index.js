import express from "express";
import db from "./Config/Database.js";
import productRoute from "./Routes/productRoute.js";
import cors from "cors";

const app = express();

try {
    await db.authenticate();
    console.log("Database Connected !");
} catch (error) {
    console.log("Database Connected error", error);
}

app.use(cors());

app.use(express.json());

app.use("/products", productRoute);

app.listen(5000, () => console.log("server running at port 5000"));