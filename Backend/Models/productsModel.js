import { Sequelize } from "sequelize";
import db from "../Config/Database.js";

const { DataTypes } = Sequelize;

const product = db.define(
    "product", {
        title: {
            type: DataTypes.STRING
        },
        price: {
            type: DataTypes.DOUBLE
        }
    }, {
        freezeTableName: true
    }
);

export default product;