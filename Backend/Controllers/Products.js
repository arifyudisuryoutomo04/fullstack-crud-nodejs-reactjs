import Product from "../Models/productsModel.js";

export const getAllProducts = async(req, res) => {
    try {
        const x = await Product.findAll();
        res.json(x);
    } catch (error) {
        res.json({ message: error.message });
    }
};

export const getProductById = async(req, res) => {
    try {
        const y = await Product.findAll({
            where: {
                id: req.params.id
            }
        });
        res.json(y[0]);
    } catch (error) {
        res.json({ message: error.message });
    }
};

export const createProducts = async(req, res) => {
    try {
        await Product.create(req.body);
        res.json({
            message: "Products Created !"
        });
    } catch (error) {
        res.json({ message: error.message });
    }
};

export const updateProduct = async(req, res) => {
    try {
        await Product.update(req.body, {
            where: {
                id: req.params.id
            }
        });
        res.json({
            message: "Products Updated !"
        });
    } catch (error) {
        res.json({ message: error.message });
    }
};

export const deleteProduct = async(req, res) => {
    try {
        await Product.destroy({
            where: {
                id: req.params.id
            }
        });
        res.json({
            message: "Products Deleted !"
        });
    } catch (error) {
        res.json({ message: error.message });
    }
};