import {useState} from 'react'
import axios from 'axios';
import {useNavigate} from 'react-router-dom'

const AddProduct = () => {
  const [title,setTitle] = useState('');
  const [price,setPrice] = useState('');
  let navigate = useNavigate();

  const saveProduct = async (e) => {
    e.preventDefault();
    await axios.post('http://localhost:5000/products',{
      title: title,
      price: price
    });
    navigate("/products", { replace: true });
  }

  return (
    <div>
      <form onSubmit={saveProduct}>
    
    <div className="field">
      <label className='label'>Nama</label>
      <input 
      type="text" 
      className='input' 
      placeholder='Nama' 
      value={title}
      onChange={(e) => setTitle(e.target.value)}
      />
    </div>
    
    <div className="field">
      <label className='label'>Harga</label>
      <input 
      type="text" 
      className='input' 
      placeholder='Harga'
      value={price}
      onChange={(e) => setPrice(e.target.value)} 
      />
    </div>
    
    <div className="field">
    <button className="button is-primary">Simpan</button>
    </div>
    
    </form>
    </div>
  )
}

export default AddProduct