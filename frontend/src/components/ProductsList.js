import { useEffect, useState } from "react";
import axios from "axios";
import { Link } from "react-router-dom";

const ProductsList = () => {
  const [products, setProduct] = useState([]);

  useEffect(() => {
    getProduct();
  }, []);

  const getProduct = async () => {
    const response = await axios.get("http://localhost:5000/products ");
    setProduct(response.data);

    // ! check API
    // console.log(response.data);
  };

  const deleteProduct = async (id) => {
    await axios.delete(`http://localhost:5000/products/${id}`);
    getProduct();
  };

  return (
    <div>
      <Link to="/products/add-product" className="button is-primary mt-2">
        Tambah Produk
      </Link>

      <table className="table is-striped is-fullwidth">
        <thead>
          <tr>
            <th>No</th>
            <th>Nama</th>
            <th>Harga</th>
            <th>Action</th>
          </tr>
        </thead>

        <tbody>
          {products.map((x, index) => (
            <tr key={x.id}>
              <th>{index + 1}</th>
              <td>{x.title}</td>
              <td>{x.price}</td>
              <td>
                <Link
                  to={`/products/edit/${x.id}`}
                  className="button is-small is-info"
                >
                  Edit
                </Link>
                <button
                  onClick={() => deleteProduct(x.id)}
                  className="button is-small is-danger"
                >
                  Delete
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default ProductsList;
